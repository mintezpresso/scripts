## Search scripts
All scripts here are written for [searchmenu](https://codeberg.org/mintezpresso/searchmenu)

Click for usage and install instructions

### Main script
- [searchmenu](https://codeberg.org/mintezpresso/Scripts/src/branch/main/search/searchmenu): extendable dmenu search engine and bookmarking system

### Modules
- [redditsearch](https://codeberg.org/mintezpresso/Scripts/src/branch/main/search/redditsearch): reddit
- [gitsearch](https://codeberg.org/mintezpresso/Scripts/src/branch/main/search/gitsearch): git
- [torsearch](https://codeberg.org/mintezpresso/Scripts/src/branch/main/search/torsearch): \*.onion
- [1337x](https://codeberg.org/mintezpresso/Scripts/src/branch/main/search/torsearch): 1337x
- [ytsmx](https://codeberg.org/mintezpresso/Scripts/src/branch/main/search/torsearch): yts.mx
