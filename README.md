# My desktop Linux shell script collection
Except for [this one](https://codeberg.org/mintezpresso/Scripts/src/branch/main/utils/spoof-dpi), most are POSIX and designed to be ran from keyboard shortcuts

For my statusbar (dwmblocks) scripts, go [here](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar)

-  Scripts are compatible with
    - ``bash`` as interactive shell
    - ``dash`` as [dwm](https://dwm.suckless.org/) SHCMD execution shell
    - ``systemd``, **except for [sysmenu](https://codeberg.org/mintezpresso/scripts/src/branch/main/menu/sysmenu)**
- Tested on Artix, Void and Debian

## Usage notes
- \*menu scripts use dmenu and need at least these dmenu patches:
    - [grid](https://tools.suckless.org/dmenu/patches/grid/) + [gridnav](https://tools.suckless.org/dmenu/patches/gridnav/)
    - [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/)
    - [exactmatch](https://libreddit.foss.wtf/r/suckless/comments/xh5kij/dmenu_is_there_a_patch_for_dmenu_to_perform_exact/)

Built into dmenu in [my DE build](https://codeberg.org/mintezpresso/ArkDE)
